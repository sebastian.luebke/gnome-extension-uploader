import os
from gnome_extension_uploader.utils import verifiy_extension_directory, get_extension_metadata

sample_extension_directory = os.path.join(os.getcwd(), "tests/sample_extension")

def test_verifiy_extension_directory_invalid():
    assert verifiy_extension_directory(os.getcwd()) is False

def test_verifiy_extension_directory_valid():
    assert verifiy_extension_directory(sample_extension_directory) is True

def test_get_extension_metadata():
    metadata = get_extension_metadata(sample_extension_directory)
    assert metadata["uuid"] == "improvedosk@luebke.io"